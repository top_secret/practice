<?php

use Illuminate\Database\Seeder;
use App\User;

class UsersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $users = array(
            ['name' => 'Himanshu Gupta' , 'email' => 'himanshurox.30@gmail.com', 'password' => bcrypt('123456'), 'phone' => '9999834390', 
             'is_super_admin' => true
            ],

            ['name' => 'CoCubes' , 'email' => 'cocubes@demo.com', 'password' => bcrypt('123456'), 'phone' => '9999834391', 
             'is_super_admin' => true
            ]
        );      

        foreach ($users as $user) {
            if (!User::whereEmail($user['email'])->exists()) {
                User::create($user);
            }
        }
    }
}
