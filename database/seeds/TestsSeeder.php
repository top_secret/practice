<?php

use Illuminate\Database\Seeder;
use App\{User, Test, Question, Option};

class TestsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $tests = array(
            [
                'name' => 'English - Sentence Completion' , 
                'description' => 'MCQ question answer English Aptitude test. Complete the sentences as appropriate.',
                'duration' => 5,
                'questions' => [
                    [
                        'question' => 'Rajeev failed in the examination because none of his answers were _______ to the questions asked.',
                        'options' => [
                            [ 'label' => 1 , 'value' => 'allusive' ],
                            [ 'label' => 2 , 'value' => 'revealing' ],
                            [ 'label' => 3 , 'value' => 'pertinent' ],
                            [ 'label' => 4 , 'value' => 'referential' ],    
                        ],
                        'correct' => 3
                    ],

                    [
                        'question' => 'There are _______ views on the issue of giving bonus to the employees.',
                        'options' => [
                            [ 'label' => 1 , 'value' => 'independent' ],
                            [ 'label' => 2 , 'value' => 'divergent' ],
                            [ 'label' => 3 , 'value' => 'modest' ],
                            [ 'label' => 4 , 'value' => 'adverse' ],    
                        ],
                        'correct' => 2
                    ],

                    [
                        'question' => 'Man who has committed such an _______ crime must get the most severe punishment.',
                        'options' => [
                            [ 'label' => 1 , 'value' => 'injurious' ],
                            [ 'label' => 2 , 'value' => 'unchritable' ],
                            [ 'label' => 3 , 'value' => 'unworhty' ],
                            [ 'label' => 4 , 'value' => 'abominable' ],    
                        ],
                        'correct' => 4
                    ],

                    [
                        'question' => 'He has _______ people visiting him at his house because he fears it will cause discomfort to neighbours',
                        'options' => [
                            [ 'label' => 1 , 'value' => 'curtailed' ],
                            [ 'label' => 2 , 'value' => 'requested' ],
                            [ 'label' => 3 , 'value' => 'stopped' ],
                            [ 'label' => 4 , 'value' => 'warned' ],    
                        ],
                        'correct' => 3
                    ],

                    [
                        'question' => 'Although he never learn to read, his exceptional memory and enquiring mind eventually made him a very _______ man.',
                        'options' => [
                            [ 'label' => 1 , 'value' => 'injurious' ],
                            [ 'label' => 2 , 'value' => 'unchritable' ],
                            [ 'label' => 3 , 'value' => 'unworhty' ],
                            [ 'label' => 4 , 'value' => 'abominable' ],    
                        ],
                        'correct' => 2
                    ],

                    [
                        'question' => 'More insurers are limiting the sale of property insurance in coastal areas and other regions _______ natural disasters.',
                        'options' => [
                            [ 'label' => 1 , 'value' => 'safe from' ],
                            [ 'label' => 2 , 'value' => 'according to' ],
                            [ 'label' => 3 , 'value' => 'despite' ],
                            [ 'label' => 4 , 'value' => 'prone to' ],    
                        ],
                        'correct' => 4
                    ],

                    [
                        'question' => 'Man who has committed such an _______ crime must get the most severe punishment.',
                        'options' => [
                            [ 'label' => 1 , 'value' => 'injurious' ],
                            [ 'label' => 2 , 'value' => 'unchritable' ],
                            [ 'label' => 3 , 'value' => 'unworhty' ],
                            [ 'label' => 4 , 'value' => 'abominable' ],    
                        ],
                        'correct' => 4
                    ],

                    [
                        'question' => 'Man who has committed such an _______ crime must get the most severe punishment.',
                        'options' => [
                            [ 'label' => 1 , 'value' => 'injurious' ],
                            [ 'label' => 2 , 'value' => 'unchritable' ],
                            [ 'label' => 3 , 'value' => 'unworhty' ],
                            [ 'label' => 4 , 'value' => 'abominable' ],    
                        ],
                        'correct' => 4
                    ],
                ]
            ],

        );  
        
        $user = User::where('is_super_admin', true)->first();
        if($user)
        {
            foreach ($tests as $test) {
                $testModel = $user->tests()->create(array_only($test , ['name', 'description', 'duration']));
                foreach($test['questions'] as $question)
                {
                    $questionModel = $testModel->questions()->create( array_only($question , ['question', 'correct']));

                    foreach($question['options'] as $option)
                    {
                        $optionModel = $questionModel->options()->create($option);   
                    }
                }

            }
        }
    }
}
