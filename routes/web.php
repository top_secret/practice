<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Auth::routes();

Route::get('/', 'HomeController@index')->name('home');


Route::middleware(['auth',])->group(function () {
    //Route::resource('tests', 'TestController');
    Route::get('/gen/{id}', 'SubmissionController@create')->name('generateTest');
    Route::get('/assess/{id}', 'TestController@startTest')->name('assess');

    Route::post('/answer', 'SubmissionController@answer')->name('answers.submit');

    Route::get('/endtest/{id}', 'SubmissionController@endTest')->name('endTest');    
    Route::get('/submissions', 'SubmissionController@index')->name('submissions');    
    
});
