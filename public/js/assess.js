$("input[type=checkbox]").change(function (e) {
    var ele = $(this);
    ele.closest('.question').find('.checkbox').removeClass('checked')
    ele.closest('.checkbox').addClass('checked');
});

$("body").on('click', '.submit-answer',  function() {
		var ele = $(this);
        var _token = $('input[name="_token"]').val();

        var data = {
            id: '{{ $submission->uuid }}',
            question: ele.data('question'),
            option: 2,
            _token: _token
        };

		$.ajax({
			type:"POST",
			url : "{{ route('answers.submit') }}",
            dataType: 'JSON',
            data: data,
			success : function(response) {
				if(response)
				{
					alert('submitted');
				}
			},
			error: function() {
				alert('Some error occured while fetching Item Details');
			}
		});

	
	});


// Set the date we're counting down to
var countDownDate = new Date("{{ $timer }}").getTime();

// Update the count down every 1 second
var x = setInterval(function() {

  // Get todays date and time
  var now = new Date().getTime();

  // Find the distance between now an the count down date
  var distance = countDownDate - now;

  // Time calculations for days, hours, minutes and seconds
  var days = Math.floor(distance / (1000 * 60 * 60 * 24));
  var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
  var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
  var seconds = Math.floor((distance % (1000 * 60)) / 1000);

  // Display the result in the element with id="demo"
  document.getElementById("time").innerHTML = minutes + "m " + seconds + "s ";

  // If the count down is finished, write some text 
  if (distance < 0) {
    clearInterval(x);
    document.getElementById("time").innerHTML = "EXPIRED";
  }
}, 1000);