@extends('layouts.main')

@section('content')

<div class="container">
    <div class="row">
        <div class="col-md-12">
            <h4 class="text-center">CoCubes Practice Test - {{ $submission->test->name }}</h4>
            <br>
           
            <div class="row">
                <div class="col-md-8 col-md-offset-2">
                    <div class="tile">
                        <center class="congrats">
                            <h1 style="font-size: 5em;color: #ef3b1f;">
                                <img src="{{ asset('img/icon-check-circle-green.svg') }}" width="100">
                            </h1>
                            <h2 style="font-size:40px;">Thank you</h2>
                            <p style="color: #042a49;width: 70%;font-size: 16px;margin-top: 30px;">Your submission is successful</p>
                            
                            <a href="{{ route('home') }}" class="btn btn-inverse">All Practice tests</a>
                            <br>
                            <br>

                        </center>

                    </div>
                    
                </div>
            </div>

           
        </div>
        
    </div>
</div>
@endsection
