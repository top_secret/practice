@extends('layouts.main')

@section('content')

<div class="container">
    <div class="row">
        <div class="col-md-12">
            <h3>CoCubes Practice Tests</h3>
            <?php 
                $array = ['yellow', 'green', 'orange', 'blue', 'purple', 'red'];
                shuffle($array);
            ?>
            <div class="row tests">
                @foreach($tests as $test)
                    <div class="col-md-3">
                        <div class="card {{ $array[$loop->index % 6] }}">
                            <div class="card-body">
                                <h4 class="truncate-normal" title="{{ $test->name }}">{{ $test->name }}</h4>
                                <p class="desc">{{ $test->description }} </p>

                                <div class="extras">
                                    <p class="question"><i class="fui-new"></i>{{ $test->questions->count() }} Questions</p>
                                    <p class="duration"><i class="fui-time"></i>{{ $test->duration }} Minutes</p>
                                </div>
                                <hr>
                                <div class="cta">
                                    <a href="{{ route('generateTest', ['id' => $test->uuid]) }}" class="btn btn-info btn-block">Practice</a>
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>   
        </div>
    </div>
</div>
@endsection
