@extends('layouts.app') @section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">Dashboard</div>

                <div class="panel-body">
                    <form action="{{ route('tests.store') }}" method="post" class="form-horizontal">
                        {{ csrf_field() }}
                        <div class="card-body">

                            <div class="form-group row">
                                <label class="col-md-2 form-control-label" for="name">Test name</label>
                                <div class="col-md-6">

                                    <input type="text" id="name" value="{{ old('name') }}" required name="name" class="form-control" placeholder="Display name">
                                    <span class="help-block"></span>
                                </div>

                            </div>

                            <div class="form-group row">
                                <label class="col-md-2 form-control-label" for="description">Description</label>
                                <div class="col-md-6">

                                    <textarea id="description" required name="description" class="form-control" placeholder="Description">{{ old('description') }}</textarea>
                                    <span class="help-block"></span>
                                </div>

                            </div>

                            <div class="panel-group" id="questions">
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <h4 class="panel-title">
                                            <a data-toggle="collapse" data-parent="#questions" href="#collapse1">Collapsible Group 1</a>
                                        </h4>
                                    </div>
                                    <div id="collapse1" class="panel-collapse collapse in">
                                        <div class="panel-body">
                                            <div class="form-group row">
                                                <label class="col-md-2 form-control-label" for="question">Question</label>
                                                <div class="col-md-6">

                                                    <input type="text" id="question" required name="question" class="form-control" placeholder="Question">
                                                    
                                                    <span class="help-block"></span>
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <h4 class="panel-title">
                                            <a data-toggle="collapse" data-parent="#questions" href="#collapse2">Collapsible Group 2</a>
                                        </h4>
                                    </div>
                                    <div id="collapse2" class="panel-collapse collapse">
                                        <div class="panel-body">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt
                                            ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation
                                            ullamco laboris nisi ut aliquip ex ea commodo consequat.</div>
                                    </div>
                                </div>
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <h4 class="panel-title">
                                            <a data-toggle="collapse" data-parent="#questions" href="#collapse3">Collapsible Group 3</a>
                                        </h4>
                                    </div>
                                    <div id="collapse3" class="panel-collapse collapse">
                                        <div class="panel-body">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt
                                            ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation
                                            ullamco laboris nisi ut aliquip ex ea commodo consequat.</div>
                                    </div>
                                </div>
                            </div>


                        </div>
                        <div class="card-footer">
                            <button type="submit" class="btn btn-sm btn-primary">
                                <i class="fa fa-dot-circle-o"></i> Submit</button>
                            <button type="reset" class="btn btn-sm btn-danger">
                                <i class="fa fa-ban"></i> Reset</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection