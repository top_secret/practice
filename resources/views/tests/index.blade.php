@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">All Tests</div>

                <div class="panel-body">
                    <table class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th>S.No</th>
                                <th>Name</th>
                                <th>Questions</th>
                                <th>Submissions</th>
                                <th>Created On</th>
                            </tr>
                        </thead>
                        @foreach($tests as $test)

                        <tr>
                            <td>{{ $loop->index + 1 }}</td>
                            <td>{{ $test->name }}</td>
                            <td>{{ $test->questions->count() }}</td>
                            <td>{{ $test->submissions->count }}</td>
                            <td>{{ $test->created_at }}</td>
                        </tr>

                         @endforeach
                        
                    </table>
                  

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
