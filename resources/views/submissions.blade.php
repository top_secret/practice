@extends('layouts.main') @section('content')

<div class="container">
    <div class="row">
        <div class="col-md-12">
            <h4 class="text-center">My Submissions</h4>
            <br>
            <div class="row">
                <div class="col-md-8 col-md-offset-2">

                    <div class="tile" style="text-align:left">
                        <table class="table table-bordered table-striped" style="font-size: 80%;">
                            <thead>
                                <tr>
                                    <th>S.No</th>
                                    <th>Test</th>
                                    <th>Total Questions</th>
                                    <th>Answered</th>

                                    <th>Given On</th>
                                </tr>
                            </thead>
                            @foreach($submissions as $submission)

                            <tr>
                                <td>{{ $loop->index + 1 }}</td>
                                <td>{{ $submission->test->name }}</td>
                                <td>{{ $submission->test->questions->count() }}</td>
                                <td>{{ $submission->answers->where('submitted', 1)->count() }}</td>
                                <td>{{ $submission->created_at }}</td>
                            </tr>

                            @endforeach

                        </table>

                        <p class="text-center">

                            <a href="{{ route('home') }}" class="btn btn-inverse">All tests</a>
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection