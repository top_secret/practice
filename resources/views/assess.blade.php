@extends('layouts.main') @section('content')

<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="timer text-center">
                <a href="#" class="timer-button">
                    <i class="fui-time"></i>&nbsp;
                    <span id="time">{{ $test->duration }}m 00s</span>
                </a>
            </div>
        </div>
    </div>
    <div class="row" id="assessment">
        <div class="col-md-9">
            <h4 class="test-heading">Test - {{ $test->name }}</h4>
            {{ csrf_field() }}

            <div class="question-box">
                @php $questionCount = count($questions); @endphp @foreach($questions as $question)
                <div id="ques{{ $loop->index + 1 }}" class="question @if($loop->index != 0) hide @else active @endif" data-question="{{ $question->id }}"
                    data-questionindex="{{ $loop->index + 1 }}" data-submitted="0">
                    <p class="text">{{ $loop->index + 1}}. {{ $question->question }}</p>

                    @foreach($question->options as $option)
                    <label class="checkbox @if($question->selected == $option->label) checked @endif" for="checkbox-{{ $question->id }}-{{ $loop->index }}">
                        <input type="checkbox" @if($question->selected == $option->label) checked @endif value="{{ $option->label }}" id="checkbox-{{ $question->id
                        }}-{{ $loop->index }}" data-toggle="checkbox" class="custom-checkbox">
                        <span class="icons">
                            <span class="icon-unchecked"></span>
                            <span class="icon-checked"></span>
                        </span>
                        {{$option->label}}. {{ $option->value }}
                    </label>
                    @endforeach
                    <div class="btn-groups">

                        @if($loop->index+1 != $questionCount)
                        <a href="javascript:void(0);" class="btn btn-primary submit-answer">Submit & Next</a>
                        <a href="javascript:void(0);" class="btn btn-danger skip">Skip</a>
                        @else
                        <a href="javascript:void(0);" class="btn btn-primary submit-answer">Submit</a>
                        @endif


                        <a href="javascript:void(0);" class="btn btn-default reset @if($question->selected == -1) hide  @endif">Reset</a>

                        <span class="pull-right answer-status ">
                            <span class="answered">{{ $answered}}</span>/{{ count($questions) }} answered
                        </span>
                        <span class="clearfix"></span>
                    </div>
                </div>
                @endforeach
            </div>
        </div>

        <div class="col-md-3">
            <div class="question-marker-section">
                <h4>Questions` status</h4>
                <div class="markers-body">
                    <div class="markers-only fixht">
                        <div class="row">
                            @foreach($questions as $question)
                            <div class="col-md-3">
                                <a href="javascript:void(0)" data-question="{{ $loop->index + 1 }}" class="btn marker @if($question->submitted == 1) btn-primary @else btn-default @endif">{{ $loop->index + 1 }}</a>
                            </div>
                            @if($loop->index>1 && $loop->index%3 == 0) <div class="col-md-12">&nbsp;</div> @endif
                            @endforeach
                        </div>
                    </div>
                    <div class="row" id="markers-desc-section">

                        <div class="col-md-12">
                            <div class="markers-desc">
                                <span class="btn btn-primary btn-xs">&nbsp;</span>
                                <span class="text">Answered&nbsp;&nbsp;</span>

                                <span class="btn btn-default btn-xs">&nbsp;</span>
                                <span class="text">Unanswered</span>
                            </div>
                        </div>



                    </div>
                </div>
            </div>
            <div class="end-test-section">
                <a href="javascript:void(0);" class="btn btn-inverse btn-block end-test">End Test</a>
            </div>
        </div>
    </div>
</div>
@endsection @push('scripts')

<script>
    $(document).ready(function () {

        function changeQuestion(id) {
            $(".question").each(function () {
                if ($(this).hasClass('active')) {
                    $(this).removeClass('active').addClass('hide');
                }

                $("#ques" + id).removeClass('hide').addClass('active');
            });
        }

        $("input[type=checkbox]").change(function (e) {
            var ele = $(this);
            ele.closest('.question').find('.checkbox').removeClass('checked')
            ele.closest('.checkbox').addClass('checked');

            var data = {
                submitted: 0
            };

            saveAnswer(data, function (err, response) {
                if (err) alert("Some error occurred");
                $(".question.active").find('.submit-answer.activeBtn').removeClass('activeBtn');
                console.log(response);
            });

            $('.question.active').find('.reset').removeClass('hide');

        });

        function saveAnswer(data, callback) {
            var _token = $('input[name="_token"]').val();
            var active = $(".question.active");

            var label = active.find('input:checked').val();
            var question = active.data('question');
            var submitted = data.submitted;

            var data = {
                id: '{{ $submission->uuid }}',
                question: question,
                option: label,
                _token: _token,
                submitted: submitted
            };

            // console.log(data)

            $.ajax({
                type: "POST",
                url: "{{ route('answers.submit') }}",
                dataType: 'JSON',
                data: data,
                success: function (response) {
                    if (response.error) {
                        callback(true, response);

                    } else {

                        if (submitted == -1) {
                            $('.question-marker-section').find('a[data-question="' + question +
                                '"]').removeClass('btn-primary').addClass('btn-default');
                        } else {
                            $('.question-marker-section').find('a[data-question="' + question +
                                '"]').removeClass('btn-default').addClass('btn-primary');
                        }

                        if (response.answered) {
                            $(".answer-status .answered").text(response.answered);
                        }

                        callback(false, response);
                    }
                },
                error: function (err, st, r) {
                    callback(true, "Server Error");
                }
            });
        }

        $("body").on('click', '.submit-answer', function () {
            var ele = $(this);

            var data = {
                submitted: 1
            };

            if ($('.question.active').find('input:checked').length == 0) {
                alert('Please select a choice first');
                return false;
            }

            saveAnswer(data, function (err, response) {
                if (err) alert("Some error occurred");
                $('.question.active').data('submitted', 1);
                $(".question.active").find('.submit-answer').addClass('activeBtn');
                var id = parseInt($('.question.active').data('questionindex'));

                if (ele.text().toLowerCase() != 'submit')
                    changeQuestion(id + 1);
                else
                    alert('Please click on End Test to submit the assessment');
                console.log(response);

                resetMarker();

            });
        });

        $("body").on('click', '.reset', function () {
            $('.question.active').find('input[type=checkbox]').prop('checked', false);
            $(".question.active").find('.submit-answer.activeBtn').removeClass('activeBtn');

            $('.question.active').find('.checkbox').removeClass('checked');

            saveAnswer({
                submitted: -1
            }, function (err, response) {
                console.log(response);
            });

            $(this).addClass('hide');

            resetMarker();
        });

        function resetMarker() {
            var ques = parseInt($(".question.active").data('questionindex'));
            $('.marker').removeClass('active-marker');
            $('.marker[data-question=' + ques + ']').addClass('active-marker');
        }

        resetMarker();

        $("body").on('click', '.marker', function () {
            var id = parseInt($(this).data('question'));

            changeQuestion(id);
            resetMarker();
        });

        $("body").on('click', '.skip', function () {
            var id = parseInt($('.question.active').data('questionindex'));
            changeQuestion(id + 1);
            resetMarker();
        });

        $("body").on('click', '.end-test', function () {
            if (confirm("Are you sure, you want to end the test?")) {
                var url = '{{ route("endTest", ["id" => $submission->uuid ]) }}';
                window.location.replace(url);
            }
            return false;
        });
    });

    // Set the date we're counting down to
    var countDownDate = new Date("{{ $timer }}").getTime();

    // Update the count down every 1 second
    var x = setInterval(function () {

        // Get todays date and time
        var now = new Date().getTime();

        // Find the distance between now an the count down date
        var distance = countDownDate - now;

        // Time calculations for days, hours, minutes and seconds
        var days = Math.floor(distance / (1000 * 60 * 60 * 24));
        var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
        var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
        var seconds = Math.floor((distance % (1000 * 60)) / 1000);

        // Display the result in the element with id="demo"
        document.getElementById("time").innerHTML = minutes + "m " + seconds + "s ";

        // If the count down is finished, write some text 
        if (distance < 0) {
            clearInterval(x);
            document.getElementById("time").innerHTML = "EXPIRED";
            var url = '{{ route("endTest", ["id" => $submission->uuid ]) }}';
                window.location.replace(url);
        }
    }, 1000);
</script>

@endpush