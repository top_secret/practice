<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Traits\{Uuids};

class Submission extends Model
{
    use Uuids;

    protected $fillable = [
        'user_id', 'test_id', 'starts_at', 'ends_at'
    ];

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function test()
    {
        return $this->belongsTo('App\Test');
    }

    public function answers()
    {
        return $this->hasMany('App\Answer');
    }
}
