<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Answer extends Model
{
    protected $fillable = [
        'question_id', 'submission_id', 'option_id', 'submitted'
    ];

    public function submission()
    {
        return $this->belongsTo('App\Submission');
    }

    public function question()
    {
        return $this->belongsTo('App\Question');
    }

    public function option()
    {
        return $this->belongsTo('App\Option');
    }
}
