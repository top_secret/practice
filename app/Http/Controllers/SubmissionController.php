<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\{Test, Submission};
use Carbon\Carbon;

class SubmissionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $submissions = auth()->user()->submissions()->with(['test.questions', 'answers'])->get();
        return view('submissions', ['submissions' => $submissions]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request, $id)
    {   
        $test = Test::whereUuid($id)->firstOrFail();
        $submission = auth()->user()->submissions()->create([
            'test_id' => $test->id,
            'starts_at' => Carbon::now(),
            'ends_at' => null
        ]);

        return redirect()->route('assess', ['id' => $submission->uuid]);  
    }

    public function answer(Request $request)
    {
        $submission = auth()->user()->submissions()->with('test.questions.options')->whereUuid($request->id)->firstOrFail();
        
        $test = $submission->test;

        if($test)
        {
            $question = $test->questions->where('id', $request->question)->first();
            if($question)
            {
                if($request->submitted == -1)
                {
                    $answer = $submission->answers()->where('question_id', $request->question)->delete();
                    return response()->json([
                        'error' => false,
                        'message' => 'Successfully updated',
                        'answered' => $submission->answers()->where('submitted', 1)->count()
                    ]);                     
                }

                $option = $question->options->where('label', $request->option)->first();

                if($option)
                {
                    $submitted = false;
        
                    if($request->submitted && $request->submitted == 1)
                        $submitted = true;
                    
                    $answer = $submission->answers()->where('question_id', $request->question)->first();
                    
                    if(!$answer)
                    {
                        $answer = $submission->answers()->create([
                            'question_id' => $request->question,
                            'option_id' => $request->option,
                            'submitted' => $submitted
                        ]);
                    }
                    else
                    {
                        $answer->submitted = $submitted;
                        $answer->option_id = $request->option;
                        $answer->save();
                    }
                    

                    if($answer)
                    {
                        return response()->json([
                            'error' => false,
                            'message' => 'Successfully saved',
                            'answered' => $submission->answers()->where('submitted', 1)->count()
                        ]);
                    }
                }
            }            
        }

        return response()->json([
            'error' => true,
            'message' => 'Server error occurred',
            
        ]);
    }

    public function endTest(Request $request, $id)
    {
        $submission = auth()->user()->submissions()->with('test.questions.options')->whereUuid($id)->firstOrFail(); 
        $submission->ends_at = Carbon::now();

        $submission->save();

        return view('endtest', ['submission' => $submission]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
