<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\{Test, Submission};
use Carbon\Carbon;

class TestController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $tests = auth()->user()->tests();
        return view('tests.index', ['tests' => $tests ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('tests.create');        
    }

    public function startTest($id)
    {
        $submission = Submission::with('answers')->whereUuid($id)->firstOrFail();
        
        $qids = [];

        if($submission->answers->count() > 0)
        $qids = $submission->answers->where('submitted', true)->pluck('question_id')->toArray();

        $test = Test::with('questions.options')->find($submission->test_id);

        $questions = [];


        foreach($test->questions as $question)
        {
            $question->submitted = false;
            $question->selected = -1;

            if(in_array($question->id , $qids))
            {
                $question->submitted = true;
                $question->selected = $submission->answers->where('submitted', true)->where('question_id', $question->id)->first()->option_id;
            }

            array_push($questions , $question);
        }

        $timer = Carbon::now()->addMinutes($test->duration)->addSeconds(5);

        return view('assess', ['test' => $test , 'submission' => $submission, 'timer' => $timer, 'questions' => $questions, 'answered' => count($qids) ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
