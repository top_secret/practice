<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Traits\{Uuids};


class Test extends Model
{
    use Uuids;

    protected $fillable = [
        'name', 'description'. 'duration'
    ];

    public function created_by()
    {
        return $this->belongsTo('App\User', 'created_by');
    }

    public function questions()
    {
        return $this->hasMany('App\Question');
    }
}
